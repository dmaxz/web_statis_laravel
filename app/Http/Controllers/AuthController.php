<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //function index berfungsi untuk mengarahkan browser agar mengakses file yang berada di folder views dengan nama "register.blade.php"
    public function register(){
        return view("register");
    }

    public function welcome(Request $request){
        //$request['var'], var hanya bisa diisi oleh name dari file yang nge-POST dan masuk ke function welcome (dalam kasus ini filenya adalah "register.blade.php")

        //function ucwords berfungsi untuk mengkapitalkan setiap huruf awal dari sebuah kata
        $nama = ucwords($request['nama_depan'] . ' ' . $request['nama_belakang']);

        //compact function berfungsi untuk melemparkan variabel yang berada di dalam function saat ini (dalam hal ini kita berada di function welcome dan akan melempar variabel bernama "nama") ke file bernama "welcome.blade.php" sehingga di dalam file welcome.blade.php sudah ada variabel bernama "nama" yang telah berisi nilai yang telah ditetapkan di function welcome ini.
        return view('welcome', compact('nama'));
    }
}
