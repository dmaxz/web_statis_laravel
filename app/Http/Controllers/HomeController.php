<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //function index berfungsi untuk mengarahkan browser agar mengakses file yang berada di folder views dengan nama "index.blade.php"
    public function index(){
        return view("index");
    }
}
