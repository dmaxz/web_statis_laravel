<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <!--Input nama-->
        <label>First name:</label><br><br>
        <input type="text" name="nama_depan"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="nama_belakang"><br><br>
        <!--Input umur-->
        <label>Umur:</label><br><br>
        <input type="number" min="1" name="umur"><br><br>
        <!--Input TL-->
        <label>Tanggal lahir:</label><br><br>
        <input type="date" name="umur"><br><br>
        <!--Input Gender-->
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="laki">Laki-laki<br>
        <input type="radio" name="gender" value="perempuan">Perempuan<br>
        <input type="radio" name="gender" value="other">Other<br><br>
        <!--Input nationality-->
        <label>Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Bruneian">Bruneian</option>
            <option value="Thai">Thai</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <!--Input language spoken-->
        <label>Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa<br>
        <input type="checkbox">Melayu<br>
        <input type="checkbox">Thai<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br>
        <br>
        <!--Input bio-->
        <label>Bio:</label><br><br>
        <textarea name="short_bio" id="" cols="30" rows="5"></textarea><br><br>
        <input type="submit" value="Sign Up!">
    </form>
</body>
</html>