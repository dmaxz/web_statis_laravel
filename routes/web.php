<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//HomeController dibuat dengan cara buka Git Bash di folder project dan kemudian mengetikkan "php artisan make:controller HomeController"
//setelah itu buatlah function di dalam HomeController.php dengan nama index agar bisa dipanggil oleh syntax dibawah ini.
//begitupula dengan AuthController

//route ke arah file bernama HomeController.php dengan function index jika user mengetikkan url awal, (pada kasus ini url awal saya "http://127.0.0.1:8000/")
Route::get('/', 'HomeController@index');

//route ke arah file bernama AuthController.php dengan function register jika user mengetikkan url /register, (pada kasus ini url awal saya "http://127.0.0.1:8000/", maka url /register tertulis di kolom url sebagai "http://127.0.0.1:8000/register")
Route::get('/register', 'AuthController@register');

//route ke arah file bernama AuthController.php dengan function welcome jika user mengetikkan url /welcome, (pada kasus ini url awal saya "http://127.0.0.1:8000/", maka url /welcome tertulis di kolom url sebagai "http://127.0.0.1:8000/welcome")
Route::post('/welcome', 'AuthController@welcome');
